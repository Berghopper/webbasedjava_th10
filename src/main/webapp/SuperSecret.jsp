<%--
  Created by IntelliJ IDEA.
  User: michiel
  Date: 23/11/2017
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SuperSecret</title>
</head>
<body>

<c:choose>
    <c:when test="${sessionScope.user != null}">

        <h1>Awesome, you made it to here ${sessionScope.user}!</h1>
        <h3>please have a look at my ripped movie collection</h3>
    </c:when>
    <c:otherwise>
        <h2>please log in first</h2>
        <c:redirect url="/login.do" />
    </c:otherwise>
</c:choose>

</body>
</html>
